---
title: Prueba interactiva
author: Alex G
date: '2020-10-01'
authors: ["El gato viudo"]
slug: prueba-interactiva
draft: no
---

Prueba de interacción

```{r}
library(ggplot2)
library(plotly)

dta <- data.frame(x=rnorm(1000), y=rnorm(1000))

plt <- ggplot(dta, aes(x=x, y=y)) + geom_point()

ggplotly(plt)
#plt
```

si actualizamos